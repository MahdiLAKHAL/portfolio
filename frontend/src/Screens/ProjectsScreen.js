import React, { useEffect, useState } from 'react';
// React Router
import { Link } from 'react-router-dom';

import { useDispatch, useSelector } from 'react-redux';
import {
  listProjects,
  saveProject,
  deleteProject,
} from '../actions/projectActions';
const ProjectsScreen = (props) => {
  const [modalVisible, setModalVisible] = useState(false);
  const [id, setId] = useState('');
  const [name, setName] = useState('');
  const [img, setImg] = useState('');
  const [technology, setTechnology] = useState('');
  const [url, setUrl] = useState('');
  const [urlSource, setUrlSource] = useState('');
  const [description, setDescription] = useState('');
  const projectList = useSelector((state) => state.projectList);
  const { loading, projects, error } = projectList;
  const projectSave = useSelector((state) => state.projectSave);
  const {
    loading: loadingSave,
    success: successSave,
    error: errorSave,
  } = projectSave;

  const projectDelete = useSelector((state) => state.projectDelete);
  const {
    loading: loadingDelete,
    success: successDelete,
    error: errorDelete,
  } = projectDelete;
  const dispatch = useDispatch();

  const submitHandler = (e) => {
    e.preventDefault();
    dispatch(
      saveProject({
        name,
        img,
        technology,
        url,
        urlSource,
        description,
        _id: id,
      })
    );
  };
  const deleteHandler = (project) => {
    dispatch(deleteProject(project._id));
  };
  useEffect(() => {
    if (successSave) {
      setModalVisible(false);
    }
    dispatch(listProjects());
  }, [successSave, successDelete]);
  const openModal = (project) => {
    setModalVisible(true);
    setId(project._id);
    setName(project.name);
    setImg(project.img);
    setTechnology(project.technology);
    setUrl(project.url);
    setUrlSource(project.urlSource);
    setDescription(project.description);
  };
  return (
    <>
      <div className='content content-margined'>
        <div className='product-header'>
          <h3>Projects</h3>
          <button className='button primary' onClick={() => openModal({})}>
            Add Project
          </button>
        </div>

        {modalVisible && (
          <div className='form'>
            <form onSubmit={submitHandler}>
              <ul className='form-container'>
                <li>
                  <h2>Add Projects</h2>
                </li>
                <li>
                  {loadingSave && <div>Loading...</div>}
                  {errorSave && <div>{errorSave}</div>}
                </li>
                <li>
                  <label htmlFor='name'> Name </label>
                  <input
                    type='text'
                    name='name'
                    id='name'
                    value={name}
                    onChange={(e) => setName(e.target.value)}
                  />
                </li>
                <li>
                  <label htmlFor='img'> Image </label>
                  <input
                    type='text'
                    name='img'
                    id='img'
                    value={img}
                    onChange={(e) => setImg(e.target.value)}
                  />
                </li>

                <li>
                  <label htmlFor='url'> Web page url </label>
                  <input
                    type='text'
                    name='url'
                    id='url'
                    value={url}
                    onChange={(e) => setUrl(e.target.value)}
                  />
                </li>
                <li>
                  <label htmlFor='urlSource'> Source code url </label>
                  <input
                    type='text'
                    name='urlSource'
                    id='urlSource'
                    value={urlSource}
                    onChange={(e) => setUrlSource(e.target.value)}
                  />
                </li>
                <li>
                  <label htmlFor='technology'> Technology </label>
                  <textarea
                    name='technology'
                    id='technology'
                    value={technology}
                    onChange={(e) => setTechnology(e.target.value)}
                  />
                </li>
                <li>
                  <label htmlFor='description'> Description </label>
                  <textarea
                    name='description'
                    id='description'
                    value={description}
                    onChange={(e) => setDescription(e.target.value)}
                  />
                </li>

                <button type='submit' className='button primary'>
                  {id ? 'Update' : 'Add new project'}
                </button>

                <button
                  type='button'
                  className='button secondary'
                  onClick={() => setModalVisible(false)}
                >
                  Back
                </button>
              </ul>
            </form>
          </div>
        )}

        <div className='product-list'>
          <table className='table'>
            <thead>
              <tr>
                <th>ID</th>
                <th>Name </th>
                <th>Technology</th>
                <th>Web page url</th>
                <th>Source code url</th>
                <th>Description</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              {projects.map((project) => (
                <tr key={project._id}>
                  <td>{project._id}</td>
                  <td>{project.name} </td>
                  <td>{project.technology}</td>
                  <td>{project.url}</td>
                  <td>{project.urlSource}</td>
                  <td>{project.description}</td>
                  <td>
                    <button
                      className='button'
                      onClick={() => openModal(project)}
                    >
                      Edit
                    </button>
                    {'  '}
                    <button
                      className='button'
                      onClick={() => deleteHandler(project)}
                    >
                      Delete
                    </button>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    </>
  );
};
export default ProjectsScreen;
