import React, { useEffect, useState } from 'react';
// React Router
import { Link } from 'react-router-dom';

import { useDispatch, useSelector } from 'react-redux';
import { signin } from '../actions/userActions';
const SigninScreen = (props) => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const userSignin = useSelector((state) => state.userSignin);
  const { loading, userInfo, error } = userSignin;
  const dispatch = useDispatch();

  const submitUser = (e) => {
    e.preventDefault();
    dispatch(signin(email, password));
  };
  useEffect(() => {
    if (userInfo) {
      props.history.push('/');
    }
  }, [userInfo]);

  return (
    <>
      <div className='form'>
        <form onSubmit={submitUser}>
          <ul className='form-container'>
            <li>
              <h2>Sign-In</h2>
            </li>
            <li>
              {loading && <div>Loading...</div>}
              {error && <div>{error}</div>}
            </li>
            <li>
              <label htmlFor='email'>Email</label>
              <input
                type='email'
                name='email'
                id='email'
                onChange={(e) => setEmail(e.target.value)}
              />
            </li>
            <li>
              <label htmlFor='password'>Password</label>
              <input
                type='password'
                name='password'
                id='password'
                onChange={(e) => setPassword(e.target.value)}
              />
            </li>
            <button type='submit' className='button primary'>
              Signin
            </button>
            <li>New in my Portofolio ?</li>
            <li>
              <Link to='/register' className='button text-center secondary'>
                Create your account
              </Link>
            </li>
          </ul>
        </form>
      </div>
    </>
  );
};
export default SigninScreen;
