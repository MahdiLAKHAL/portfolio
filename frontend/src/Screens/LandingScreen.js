import React, { useEffect } from 'react';

// React Router
import { Link } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { listProjects } from '../actions/projectActions';
const LandingScreen = (props) => {
  const projectList = useSelector((state) => state.projectList);
  const { projects, loading, error } = projectList;
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(listProjects());
    return () => {
      //
    };
  }, []);
  return (
    <>
      {loading ? (
        <h3> Loading ... </h3>
      ) : error ? (
        <div>{error}</div>
      ) : (
        <ul className='products'>
          {projects.map((project) => (
            <li key={project._id}>
              <div className='product'>
                <Link to={'/details/' + project._id}>
                  <img
                    className='product-image'
                    src={project.img}
                    alt='product'
                  />
                </Link>

                <div className='product-name'>
                  <Link to={'/details/' + project._id}>{project.name}</Link>
                </div>
                <div className='product-brand'>{project.technology}</div>
                <div className='product-brand'>
                  <a href={project.urlSource}>Check the Source</a>
                </div>
                <div className='product-rating'>{project.views} views</div>
              </div>
            </li>
          ))}
        </ul>
      )}
    </>
  );
};

export default LandingScreen;
