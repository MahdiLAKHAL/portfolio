import React, { useEffect, useState } from 'react';
import { detailsProject } from '../actions/projectActions';
// React Router
import { Link } from 'react-router-dom';

import { BiCodeAlt, BiPlanet } from 'react-icons/bi';
import { useDispatch, useSelector } from 'react-redux';

const ProjectScreen = (props) => {
  const dispatch = useDispatch();
  const projectId = props.match.params.id;
  const [note, setNote] = useState(1);
  const projectDetails = useSelector((state) => state.projectDetails);
  const { project, loading, error } = projectDetails;

  useEffect(() => {
    dispatch(detailsProject(projectId));
    return () => {
      //
    };
  }, []);

  const handleSubmitNote = () => {
    props.history.push('/cart/' + props.match.params.id + '?note=' + note);
  };

  return (
    <>
      <div className='back-to-result'>
        <Link to='/'>Back to Home</Link>
      </div>
      {loading ? (
        <h4>Loading ...</h4>
      ) : error ? (
        <h4>{error}</h4>
      ) : (
        <div className='details'>
          <div className='details-image'>
            <img src={project.img} alt='project' />
          </div>
          <div className='details-info'>
            <ul>
              <li>
                <h3>{project.name}</h3>
              </li>
              <li>
                <h4>Technologies Applied :</h4> <h5>{project.technology}</h5>{' '}
              </li>

              <li>
                <h4>Description :</h4> <p>{project.description}</p>
              </li>
              <li>
                <div>
                  <a href={project.url}>
                    <BiPlanet></BiPlanet>
                    <b>Show me the webPage</b>
                  </a>
                  <br />
                  <a href={project.urlSource}>
                    <BiCodeAlt></BiCodeAlt>{' '}
                    <b> Let me check the source code </b>
                  </a>
                </div>
              </li>
            </ul>
            <div className='details-action'>
              <ul>
                <li>
                  Give a note :
                  <select
                    value={note}
                    id='select'
                    onChange={(e) => {
                      setNote(e.target.value);
                    }}
                  >
                    {[...Array(project.noteUser).keys()].map((x) => (
                      <option key={x + 1} value={x + 1}>
                        {x + 1}
                      </option>
                    ))}
                  </select>
                  /5
                </li>
                <li>
                  <button onClick={handleSubmitNote} className='button primary'>
                    Submit your note
                  </button>
                </li>
              </ul>
            </div>
          </div>
        </div>
      )}
    </>
  );
};
export default ProjectScreen;
