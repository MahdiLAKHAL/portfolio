import React, { useState, useEffect } from 'react';
import { addToCart, removeFromCart } from '../actions/cartActions';
import { useDispatch, useSelector } from 'react-redux';
import { BiCodeAlt, BiPlanet } from 'react-icons/bi';
import { FaTimes } from 'react-icons/fa';
import { Link } from 'react-router-dom';
const CartScreen = (props) => {
  // Modal
  const [isModalOpen, setIsModalOpen] = useState(false);
  function closeModal() {
    setIsModalOpen(false);
  }
  //
  const cart = useSelector((state) => state.cart);
  const { cartItems } = cart;
  const projectID = props.match.params.id;
  const note = props.location.search
    ? Number(props.location.search.split('=')[1])
    : 0;
  const dispatch = useDispatch();
  const removeFromCarteHandler = (projectID) => {
    dispatch(removeFromCart(projectID));
  };
  useEffect(() => {
    if (projectID) {
      dispatch(addToCart(projectID, note));
    }
  }, []);

  const submitHandler = () => {
    setIsModalOpen(true);
  };

  return (
    <>
      <div className='cart'>
        <div className='cart-list'>
          <ul className='cart-list-container'>
            <li>
              <h3>Projects noted</h3>
            </li>
            {cartItems.length === 0 ? (
              <div>Cart is Empty</div>
            ) : (
              cartItems.map((item) => (
                <li key={item.project}>
                  <div className='cart-image'>
                    <Link to={'/details/' + item.project}>
                      <img src={item.image} alt='project' />
                    </Link>
                  </div>
                  <div className='cart-name'>
                    <b> {item.name}</b> <br />
                    <b>{item.technology}</b>
                    <br />
                    <a href={item.url}>
                      <BiPlanet></BiPlanet>
                      <b>To the webPage</b>
                    </a>
                    <br />
                    <a href={item.urlSource}>
                      <BiCodeAlt></BiCodeAlt>
                      <b> To the source code </b>
                    </a>
                  </div>
                  <div className='cart-price'>
                    <div>Note :{item.note}</div>
                    <button
                      type='button'
                      onClick={() => removeFromCarteHandler(item.project)}
                      className='button'
                    >
                      Delete
                    </button>
                  </div>
                </li>
              ))
            )}
          </ul>
        </div>
        <div className='cart-action'>
          <h3>
            Grade :{' '}
            {cartItems.reduce((a, c) => a + c.note, 0) / cartItems.length}/ 5
          </h3>
          <button
            className='button primary full-width'
            disabled={cartItems.length === 0}
            onClick={submitHandler}
          >
            Submit
          </button>
        </div>
      </div>
      <div
        className={`${
          isModalOpen ? 'modal-overlay show-modal' : 'modal-overlay'
        }`}
      >
        <div className='modal-container'>
          <h3> Summary </h3>
          <div>
            <div className='cart'>
              <div className='cart-list'>
                <ul className='cart-list-container'>
                  <li>
                    <h3>Projects noted</h3>
                  </li>
                  {cartItems.length === 0 ? (
                    <div>Cart is Empty</div>
                  ) : (
                    cartItems.map((item) => (
                      <li key={item.project}>
                        <div className='cart-image'>
                          <Link to={'/details/' + item.project}>
                            <img src={item.image} alt='project' />
                          </Link>
                        </div>
                        <div className='cart-name'>
                          <b> {item.name}</b> <br />
                          <b>{item.technology}</b>
                        </div>
                        <div className='cart-price'>Grade :{item.note}</div>
                      </li>
                    ))
                  )}
                  <h3>
                    Final Grade :
                    {cartItems.reduce((a, c) => a + c.note, 0) /
                      cartItems.length}
                    / 5
                  </h3>
                </ul>
              </div>
            </div>
            <button className='close-modal-btn' onClick={closeModal}>
              <FaTimes></FaTimes>
            </button>
          </div>
        </div>
      </div>
    </>
  );
};

export default CartScreen;
