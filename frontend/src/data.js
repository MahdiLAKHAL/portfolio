export default {
  projects: [
    {
      _id: '1',
      name: 'Dark Look',
      img: '/images/meanStack.png',
      technology: 'Angular , MongoDB, Express js, Node.js, Bootstrap',
      url: '',
      urlSource: 'https://github.com/Mahdi-LAKHAL/WatchShop',
      views: 12,
      description:
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
    },
    {
      _id: '2',
      name: 'Confusion',
      img: '/images/bootstrap-social-logo.png',
      technology: 'html, css, javaScript, jQuery, Bootstrap',
      url: '',
      urlSource: 'https://bitbucket.org/MahdiLAKHAL/bootstrap4/src/master/',
      views: 13,
      description:
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
    },
    {
      _id: '3',
      name: 'React Basics',
      img: '/images/react.png',
      technology: 'React JS',
      url: '',
      urlSource:
        'https://bitbucket.org/MahdiLAKHAL/reactjs-advanced/src/master/',
      views: 16,
      description:
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
    },
    {
      _id: '4',
      name: 'Birthday Reminder',
      img: '/images/react.png',
      technology: 'React JS',
      url: '',
      urlSource: 'https://bitbucket.org/MahdiLAKHAL/birthday-list/src/master/',
      views: 10,
      description:
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
    },
    {
      _id: '5',
      name: 'City Tours',
      img: '/images/react.png',
      technology: 'React JS',
      url: '',
      urlSource: 'https://bitbucket.org/MahdiLAKHAL/city-tours/src/master/',
      views: 8,
      description:
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
    },
    {
      _id: '6',
      name: 'Reviews',
      img: '/images/react.png',
      technology: 'React JS',
      url: '',
      urlSource: 'https://bitbucket.org/MahdiLAKHAL/reviews/src/master/',
      views: 12,
      description:
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
    },
    {
      _id: '7',
      name: 'Accordion',
      img: '/images/react.png',
      technology: 'React JS',
      url: '',
      urlSource: 'https://bitbucket.org/MahdiLAKHAL/accordion/src/master/',
      views: 9,
      description:
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
    },
    {
      _id: '8',
      name: 'Tabs',
      img: '/images/react.png',
      technology: 'React JS',
      url: '',
      urlSource: 'https://bitbucket.org/MahdiLAKHAL/tabs/src/master/',
      views: 11,
      description:
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
    },
    {
      _id: '9',
      name: 'Grocery Bud',
      img: '/images/react.png',
      technology: 'React JS',
      url: '',
      urlSource: 'https://bitbucket.org/MahdiLAKHAL/grocery-bud/src/master/',
      views: 10,
      description:
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
    },
    {
      _id: '10',
      name: 'Responsive Navbar',
      img: '/images/react.png',
      technology: 'React JS',
      url: '',
      urlSource: 'https://bitbucket.org/MahdiLAKHAL/navbar/src/master/',
      views: 12,
      description:
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
    },
    {
      _id: '11',
      name: 'Sidebar and Modal',
      img: '/images/react.png',
      technology: 'React JS',
      url: '',
      urlSource: 'https://bitbucket.org/MahdiLAKHAL/sidebar-modals/src/master/',
      views: 8,
      description:
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
    },
    {
      _id: '12',
      name: 'Stripe Menu',
      img: '/images/react.png',
      technology: 'React JS',
      url: '',
      urlSource: 'https://bitbucket.org/MahdiLAKHAL/stripe-menu/src/master/',
      views: 12,
      description:
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
    },
    {
      _id: '13',
      name: 'Cart Items',
      img: '/images/react.png',
      technology: 'React JS',
      url: '',
      urlSource: 'https://bitbucket.org/MahdiLAKHAL/cart-shop/src/master/',
      views: 15,
      description:
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
    },
  ],
};
