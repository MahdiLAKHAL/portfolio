import Axios from 'axios';
import {
  PROJECT_DETAILS_REQUEST,
  PROJECT_DETAILS_SUCCESS,
  PROJECT_DETAILS_FAIL,
  PROJECT_LIST_FAIL,
  PROJECT_LIST_REQUEST,
  PROJECT_LIST_SUCCESS,
  PROJECT_SAVE_REQUEST,
  PROJECT_SAVE_SUCCESS,
  PROJECT_SAVE_FAIL,
  PROJECT_DELETE_REQUEST,
  PROJECT_DELETE_SUCCESS,
  PROJECT_DELETE_FAIL,
} from '../constants/projectConstants/projectConstants';

const listProjects = () => async (dispatch) => {
  try {
    dispatch({ type: PROJECT_LIST_REQUEST });
    const { data } = await Axios.get('/api/projects');
    dispatch({ type: PROJECT_LIST_SUCCESS, payload: data });
  } catch (error) {
    dispatch({ type: PROJECT_LIST_FAIL, payload: error.message });
  }
};
const saveProject = (project) => async (dispatch, getState) => {
  dispatch({ type: PROJECT_SAVE_REQUEST, payload: project });
  const {
    userSignin: { userInfo },
  } = getState();
  try {
    if (!project._id) {
      const { data } = await Axios.post('/api/projects', project, {
        headers: {
          Authorization: 'Bearer' + userInfo.token,
        },
      });
      dispatch({ type: PROJECT_SAVE_SUCCESS, payload: data.project });
    } else {
      const { data } = await Axios.put(
        '/api/projects/' + project._id,
        project,
        {
          headers: {
            Authorization: 'Bearer' + userInfo.token,
          },
        }
      );
      dispatch({ type: PROJECT_SAVE_SUCCESS, payload: data.project });
    }
  } catch (error) {
    dispatch({ type: PROJECT_SAVE_FAIL, payload: error.message });
  }
};

const detailsProject = (projectId) => async (dispatch) => {
  dispatch({ type: PROJECT_DETAILS_REQUEST, payload: projectId });
  try {
    const { data } = await Axios.get('/api/details/' + projectId);
    dispatch({ type: PROJECT_DETAILS_SUCCESS, payload: data });
  } catch (error) {
    dispatch({ type: PROJECT_DETAILS_FAIL, payload: error.message });
  }
};
const deleteProject = (projectId) => async (dispatch, getState) => {
  const {
    userSignin: { userInfo },
  } = getState();
  try {
    dispatch({ type: PROJECT_DELETE_REQUEST, payload: projectId });
    const { data } = await Axios.delete('/api/projects/' + projectId, {
      headers: {
        Authorization: 'Bearer' + userInfo.token,
      },
    });
    dispatch({ type: PROJECT_DELETE_SUCCESS, payload: data, succes: true });
  } catch (error) {
    dispatch({ type: PROJECT_DELETE_FAIL, payload: error.message });
  }
};
export { listProjects, detailsProject, saveProject, deleteProject };
