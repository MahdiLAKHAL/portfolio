import axios from 'axios';
import Cookie from 'js-cookie';
import { CART_ADD_ITEM, CART_REMOVE_ITEM } from '../constants/cartConstants';
const addToCart = (projectID, note) => async (dispatch, getState) => {
  try {
    const { data } = await axios.get('/api/details/' + projectID);
    dispatch({
      type: CART_ADD_ITEM,
      payload: {
        project: data._id,
        name: data.name,
        image: data.img,
        url: data.url,
        urlSource: data.urlSource,
        technology: data.technology,
        note,
      },
    });
    const {
      cart: { cartItems },
    } = getState();
    Cookie.set('cartItems', JSON.stringify(cartItems));
  } catch (error) {}
};

const removeFromCart = (projectID) => (dispatch, getState) => {
  dispatch({ type: CART_REMOVE_ITEM, payload: projectID });
  const {
    cart: { cartItems },
  } = getState();
  Cookie.set('cartItems', JSON.stringify(cartItems));
};
export { addToCart, removeFromCart };
