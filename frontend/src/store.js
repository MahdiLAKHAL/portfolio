import { compose, combineReducers, applyMiddleware, createStore } from 'redux';
import thunk from 'redux-thunk';
import Cookie from 'js-cookie';
import {
  projectDeleteReducer,
  projectDetailsReducer,
  projectListReducer,
  projectSaveReducer,
} from './reducers/projectReducers';
import { cartReducer } from './reducers/cartReducers';
import {
  userRegisterReducer,
  userSigninReducer,
} from './reducers/userReducers';

const cartItems = Cookie.getJSON('cartItems') || [];
const userInfo = Cookie.getJSON('userInfo') || null;
const initialState = { cart: { cartItems }, userSignin: { userInfo } };

const reducer = combineReducers({
  projectList: projectListReducer,
  projectDetails: projectDetailsReducer,
  projectSave: projectSaveReducer,
  projectDelete: projectDeleteReducer,
  cart: cartReducer,
  userSignin: userSigninReducer,
  userRegister: userRegisterReducer,
});

// thunnk allows us to apply async function in redux
const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(
  reducer,
  initialState,
  composeEnhancer(applyMiddleware(thunk))
);
export default store;
