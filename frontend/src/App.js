import React, { useState } from 'react';

// Screens
import LandingScreen from './Screens/LandingScreen';
import ProjectScreen from './Screens/ProjectScreen';
import CartScreen from './Screens/CartScreen';
import SigninScreen from './Screens/SigninScreen';
import RegisterScreen from './Screens/RegisterScreen';
import ProjectsScreen from './Screens/ProjectsScreen';
//icons
import {
  FaTimes,
  FaGithub,
  FaBitbucket,
  FaFacebook,
  FaLinkedin,
} from 'react-icons/fa';
import { BiCodeAlt } from 'react-icons/bi';
// React Router
import { BrowserRouter, Route, Link } from 'react-router-dom';
import { useSelector } from 'react-redux';

function App() {
  const userSignin = useSelector((state) => state.userSignin);
  const { userInfo } = userSignin;
  const [isSidebarOpen, setIsSidebarOpen] = useState(false);
  const openMenu = () => {
    setIsSidebarOpen(true);
  };
  const closeMenu = () => {
    setIsSidebarOpen(false);
  };
  return (
    <BrowserRouter>
      <div className='grid-container'>
        <header className='header'>
          <div className='brand'>
            <button onClick={openMenu}>&#9776;</button>
            <Link to='/'>mahdilakhal</Link>
          </div>

          <div className='header-links'>
            <a href='https://bitbucket.org/MahdiLAKHAL/portfolio/src/master/'>
              <BiCodeAlt></BiCodeAlt> source code
            </a>
            <Link to='/cart/'>Favorite </Link>
            {userInfo ? (
              <Link to='/projects'>{userInfo.name}'s Dashboard</Link>
            ) : (
              <Link to='/signin'>Sign In</Link>
            )}
          </div>
        </header>
        <aside
          className={`${isSidebarOpen ? 'sidebar show-sidebar' : 'sidebar'}`}
        >
          <div className='sidebar-header'>
            <img src='./images/image1.jpg' alt='logo' className='logo' />
            <h1> Mahdi LAKHAL </h1>
            <h4>Web Developer JUNIOR</h4>
            <button className='close-btn' onClick={closeMenu}>
              <FaTimes></FaTimes>
            </button>
          </div>
          <ul className='links'>
            <li>
              {' '}
              <p>
                Initially graduated as a civil engineer, I look forward to
                achieve several goals and evolving. I think that my curiosity
                and passion for innovative ideas and creative aspects of work
                helped me to make my way in the field of Information Technology
                that I
              </p>{' '}
            </li>
            <li>
              <h4> My Portolio</h4>
              <p>
                I would like to thank you for the time you devoted to me during
                our meeting on February 26 and for the transparency of our
                discussion. This reinforced my motivation to land the job of a
                web developer at <b>AkeoPlus</b> . Following this exchange, I
                allow myself to come back to you to remind you of the challenge
                we have set and I hope that you once again offer me a little of
                your time and your consideration to see where I am in relation
                to this challenge of learning React in this short period. To
                begin with, I am sharing with you a project in the{' '}
                <b>MERN stack</b> that I carried out recently in order to
                consolidate the achievements of the various trainings that I
                carried out to confirm my interest in this position and to
                facilitate my evaluation.
              </p>
              <a href='https://bitbucket.org/MahdiLAKHAL/portfolio/src/master/'>
                <BiCodeAlt></BiCodeAlt> source code
              </a>
            </li>
          </ul>
          <ul className='social-icons'>
            <li>
              <a href='https://www.facebook.com/mahdi.lakhal.39/'>
                <FaFacebook></FaFacebook>
              </a>
              <a href='https://www.linkedin.com/in/mahdi-lakhal/'>
                <FaLinkedin></FaLinkedin>
              </a>
              <a href='https://github.com/Mahdi-LAKHAL'>
                <FaGithub></FaGithub>
              </a>
              <a href='https://bitbucket.org/MahdiLAKHAL/'>
                <FaBitbucket></FaBitbucket>
              </a>
            </li>
          </ul>
        </aside>
        <main className='main'>
          <div className='content'>
            <Route path='/projects' component={ProjectsScreen} />
            <Route path='/signin' component={SigninScreen} />
            <Route path='/register' component={RegisterScreen} />
            <Route path='/details/:id' component={ProjectScreen} />
            <Route path='/cart/:id?' component={CartScreen} />
            <Route path='/' exact={true} component={LandingScreen} />
          </div>
        </main>
        <footer className='footer'>
          <div className='footer-links'>
            <a href='https://www.facebook.com/mahdi.lakhal.39/'>
              <FaFacebook></FaFacebook> FaceBook
            </a>
            <a href='https://www.linkedin.com/in/mahdi-lakhal/'>
              <FaLinkedin></FaLinkedin> LinkedIn
            </a>
            <a href='https://bitbucket.org/MahdiLAKHAL/'>
              <FaBitbucket></FaBitbucket> BitBucket
            </a>
            <a href='https://github.com/Mahdi-LAKHAL'>
              <FaGithub></FaGithub> GitHub
            </a>
          </div>
        </footer>
      </div>
    </BrowserRouter>
  );
}

export default App;
