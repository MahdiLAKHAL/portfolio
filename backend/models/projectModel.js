import mongoose from 'mongoose';

const projectSchema = new mongoose.Schema({
  name: { type: String, required: true },
  img: { type: String, required: true },
  technology: { type: String, required: true },
  url: { type: String, required: true },
  urlSource: { type: String, required: true },
  views: { type: Number, default: 0, required: false },
  description: { type: String, required: true },
  noteUser: { type: Number, default: 5, required: false },
});

const projectModel = mongoose.model('project', projectSchema);

export default projectModel;
