import express from 'express';
import Project from '../models/projectModel';

const router = express.Router();

router.get('/', async (req, res) => {
  const projects = await Project.find({});

  res.send(projects);
});

router.post('/', async (req, res) => {
  const project = new Project({
    name: req.body.name,
    img: req.body.img,
    technology: req.body.technology,
    url: req.body.url,
    urlSource: req.body.urlSource,
    views: req.body.views,
    description: req.body.description,
    noteUser: req.body.noteUser,
  });

  const newProject = await project
    .save()
    .then((result) => {
      res.status(201).json({ message: 'new project created', result: result });
    })
    .catch((err) => {
      res.status(500).json({
        error: err,
      });
    });
});

router.put('/:id', async (req, res) => {
  const projectId = req.params.id;
  const project = await Project.findById(projectId);
  if (project) {
    project.name = req.body.name;
    project.img = req.body.img;
    project.technology = req.body.technology;
    project.url = req.body.url;
    project.urlSource = req.body.urlSource;
    project.views = req.body.views;
    project.description = req.body.description;
    project.noteUser = req.body.noteUser;
    const updatedProject = await project
      .save()
      .then((result) => {
        res.status(200).json({ message: 'project updated', result: result });
      })
      .catch((err) => {
        res.status(500).json({
          error: err,
        });
      });
  } else {
    res.status(500).send({ message: 'Error in updating project' });
  }
});

router.delete('/:id', async (req, res) => {
  const deleteProject = await Project.findById(req.params.id);
  if (deleteProject) {
    await deleteProject.remove();
    res.send({ msg: 'project Deleted' });
  } else {
    res.send('Error in deletion');
  }
});
export default router;
