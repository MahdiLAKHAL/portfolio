import express from 'express';
import data from './data';
import config from './config';
import dotenv from 'dotenv';
import mongoose from 'mongoose';
import bodyParser from 'body-parser';
import userRoute from './routes/userRoute';
import projectRoute from './routes/projectRoute';
import Project from './models/projectModel';

dotenv.config();
const mongodbUrl = config.MONGODB_URL;
mongoose
  .connect(mongodbUrl, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
  })
  .catch((error) => console.log(error.reason));

const app = express();
app.use(bodyParser.json());
app.use('/api/users', userRoute);
app.use('/api/projects', projectRoute);
// app.use('/api/details/:id', projectRoute);

// app.get('/api/projects', (req, res) => {
//   res.send(data.projects);
// });

// app.get('/api/details/:id', (req, res) => {
//   const projectID = req.params.id;
//   const project = data.projects.find((x) => x._id === projectID);
//   if (project) {
//     res.send(project);
//   } else {
//     res.status(404).send({ msg: 'Project Not Found' });
//   }
// });

app.get('/api/details/:id', async (req, res) => {
  const project = await Project.findById(req.params.id);
  if (project) {
    res.send(project);
  } else {
    res.status(404).send({ msg: 'Project Not Found' });
  }
});

app.listen(5000, () => {
  console.log('Server started at http://localhost:5000');
});
